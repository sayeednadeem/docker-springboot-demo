FROM openjdk:8
ADD target/docker-test-project.jar docker-test-project.jar
EXPOSE 8086
ENTRYPOINT ["java", "-jar", "/docker-test-project.jar"]