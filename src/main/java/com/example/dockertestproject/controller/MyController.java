package com.example.dockertestproject.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
	
	@RequestMapping("/docker")
	public String getMessage() {
		return "Hello Docker!!!";
				
	}

}
